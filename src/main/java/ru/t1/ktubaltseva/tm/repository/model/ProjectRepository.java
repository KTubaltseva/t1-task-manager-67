package ru.t1.ktubaltseva.tm.repository.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.ktubaltseva.tm.model.Project;

@Repository
@Scope("prototype")
public interface ProjectRepository extends AbstractRepository<Project> {

}
