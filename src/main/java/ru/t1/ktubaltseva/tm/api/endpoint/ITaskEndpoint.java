package ru.t1.ktubaltseva.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/tasks")
public interface ITaskEndpoint {

    @NotNull
    @WebMethod
    @PutMapping("/create")
    Task create() throws AbstractException;

    @NotNull
    @WebMethod
    @PutMapping("/add")
    Task add(
            @WebParam(name = "task", partName = "task")
            @RequestBody @NotNull Task task
    ) throws AbstractException;

    @WebMethod
    @DeleteMapping("/delete/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull String id
    ) throws AbstractException;

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull String id
    ) throws AbstractException;

    @NotNull
    @WebMethod
    @GetMapping("/findById/{id}")
    Task findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull String id
    ) throws AbstractException;

    @NotNull
    @WebMethod
    @PostMapping("/update/{id}")
    Task update(
            @WebParam(name = "task", partName = "task")
            @RequestBody @NotNull Task task
    ) throws AbstractException;

    @NotNull
    @WebMethod
    @GetMapping("/findAll")
    List<Task> findAll();

    @WebMethod
    @DeleteMapping("/clear")
    void clear();

    @WebMethod
    @GetMapping("/count")
    long count();

}
