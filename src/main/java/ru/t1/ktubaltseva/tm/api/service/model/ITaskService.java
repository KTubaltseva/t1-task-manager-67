package ru.t1.ktubaltseva.tm.api.service.model;

import ru.t1.ktubaltseva.tm.model.Task;

public interface ITaskService extends IService<Task> {

}
