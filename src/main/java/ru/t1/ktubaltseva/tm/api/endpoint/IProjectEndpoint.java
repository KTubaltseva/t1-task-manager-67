package ru.t1.ktubaltseva.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/projects")
public interface IProjectEndpoint {

    @NotNull
    @WebMethod
    @PutMapping("/create")
    Project create() throws AbstractException;

    @NotNull
    @WebMethod
    @PutMapping("/add")
    Project add(
            @WebParam(name = "project", partName = "project")
            @RequestBody @NotNull Project project
    ) throws AbstractException;

    @WebMethod
    @DeleteMapping("/delete/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull String id
    ) throws AbstractException;

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull String id
    ) throws AbstractException;

    @NotNull
    @WebMethod
    @GetMapping("/findById/{id}")
    Project findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull String id
    ) throws AbstractException;

    @NotNull
    @WebMethod
    @PostMapping("/update/{id}")
    Project update(
            @WebParam(name = "project", partName = "project")
            @RequestBody @NotNull Project project
    ) throws AbstractException;

    @NotNull
    @WebMethod
    @GetMapping("/findAll")
    List<Project> findAll();

    @WebMethod
    @DeleteMapping("/clear")
    void clear();

    @WebMethod
    @GetMapping("/count")
    long count();

}
