package ru.t1.ktubaltseva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.ktubaltseva.tm.api.endpoint.IProjectEndpoint;
import ru.t1.ktubaltseva.tm.api.service.model.IProjectService;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.t1.ktubaltseva.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint implements IProjectEndpoint {

    @Autowired
    private IProjectService service;

    @NotNull
    @Override
    @WebMethod
    @PutMapping("/create")
    public Project create() throws AbstractException {
        return service.create();
    }

    @NotNull
    @Override
    @WebMethod
    @PutMapping("/add")
    public Project add(
            @WebParam(name = "project", partName = "project")
            @RequestBody @NotNull final Project project
    ) throws AbstractException {
        return service.create(project);
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException {
        service.deleteById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException {
        return service.existsById(id);
    }

    @NotNull
    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public Project findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException {
        return service.findById(id);
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/update/{id}")
    public Project update(
            @WebParam(name = "project", partName = "project")
            @RequestBody @NotNull final Project project
    ) throws AbstractException {
        return service.update(project);
    }

    @NotNull
    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return service.findAll();
    }

    @Override
    @WebMethod
    @DeleteMapping("/clear")
    public void clear() {
        service.clear();
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return service.count();
    }

}

