package ru.t1.ktubaltseva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.ktubaltseva.tm.api.endpoint.ITaskEndpoint;
import ru.t1.ktubaltseva.tm.api.service.model.ITaskService;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.t1.ktubaltseva.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint implements ITaskEndpoint {

    @Autowired
    private ITaskService service;

    @NotNull
    @Override
    @WebMethod
    @PutMapping("/create")
    public Task create() throws AbstractException {
        return service.create();
    }

    @NotNull
    @Override
    @WebMethod
    @PutMapping("/add")
    public Task add(
            @WebParam(name = "task", partName = "task")
            @RequestBody @NotNull final Task task
    ) throws AbstractException {
        return service.create(task);
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException {
        service.deleteById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException {
        return service.existsById(id);
    }

    @NotNull
    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public Task findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException {
        return service.findById(id);
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/update/{id}")
    public Task update(
            @WebParam(name = "task", partName = "task")
            @RequestBody @NotNull final Task task
    ) throws AbstractException {
        return service.update(task);
    }

    @NotNull
    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<Task> findAll() {
        return service.findAll();
    }


    @Override
    @WebMethod
    @DeleteMapping("/clear")
    public void clear() {
        service.clear();
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return service.count();
    }

}

