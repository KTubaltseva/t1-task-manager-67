package ru.t1.ktubaltseva.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ktubaltseva.tm.api.service.model.ITaskService;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.repository.model.TaskRepository;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class TaskService extends AbstractService<Task, TaskRepository> implements ITaskService {

    @NotNull
    @Autowired
    private TaskRepository repository;

    @Override
    public @NotNull Task create() throws EntityNotFoundException {
        return create(new Task());
    }

}
