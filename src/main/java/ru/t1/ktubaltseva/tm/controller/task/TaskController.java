package ru.t1.ktubaltseva.tm.controller.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.ktubaltseva.tm.api.service.model.IProjectService;
import ru.t1.ktubaltseva.tm.api.service.model.ITaskService;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.model.Task;

@Controller
public class TaskController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @GetMapping("/task/create")
    public String create() throws EntityNotFoundException {
        taskService.create();
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(@PathVariable("id") final String id) throws EntityNotFoundException, IdEmptyException {
        taskService.deleteById(id);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(@PathVariable("id") final String id) throws EntityNotFoundException, IdEmptyException {
        @NotNull final Task task = taskService.findById(id);
        @NotNull final ModelAndView modelAndView = new ModelAndView("task/task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("statuses", Status.values());
        modelAndView.addObject("projects", projectService.findAll());
        return modelAndView;
    }

    @PostMapping("/task/edit/{id}")
    public String edit(@ModelAttribute("task") Task task,
                       BindingResult result
    ) throws EntityNotFoundException {
        taskService.create(task);
        return "redirect:/tasks";
    }

}
