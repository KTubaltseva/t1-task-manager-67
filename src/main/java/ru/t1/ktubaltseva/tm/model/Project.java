package ru.t1.ktubaltseva.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "tm_project")
public final class Project extends AbstractModel {

    private static final long serialVersionUID = 0;

    public Project(@Nullable String name) {
        super(name);
    }

    public Project() {
        super();
    }

}
